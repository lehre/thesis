\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{idial-thesis}[2023-03-01 v1.0.0 IDiAL thesis class]
% Language options
\DeclareOption{ngerman}{\PassOptionsToPackage{\CurrentOption}{babel}}
\DeclareOption{english}{\PassOptionsToPackage{main=\CurrentOption,ngerman}{babel}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrbook}}
\ProcessOptions\relax

% used to detect language families
\RequirePackage{translations}

\LoadClass{scrbook}

%% VARIABLES
\newcommand{\theidnumber}{}
\newcommand{\idnumber}[1]{\renewcommand{\theidnumber}{#1}}
\newcommand{\thebirthday}{}
\newcommand{\birthday}[1]{\renewcommand{\thebirthday}{#1}}
\newcommand{\theplace}{}
\newcommand{\place}[1]{\renewcommand{\theplace}{#1}}

\newcommand{\thereviewerone}{}
\newcommand{\thereviewertwo}{}
\newcommand{\reviewerone}[1]{\renewcommand{\thereviewerone}{#1}}
\newcommand{\reviewertwo}[1]{\renewcommand{\thereviewertwo}{#1}}

\newcommand{\thekind}{}
\newcommand{\kind}[1]{\renewcommand{\thekind}{#1}}

\newcommand{\thedepartment}{}
\newcommand{\department}[1]{\renewcommand{\thedepartment}{#1}}
\newcommand{\thestudyprogramme}{}
\newcommand{\studyprogramme}[1]{\renewcommand{\thestudyprogramme}{#1}}
\newcommand{\thespecialization}{}
\newcommand{\specialization}[1]{\renewcommand{\thespecialization}{#1}}
\newcommand{\thedegree}{}
\newcommand{\degree}[1]{\renewcommand{\thedegree}{#1}}

%% TYPOGRAPHY

% T1 font encoding
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}

\RequirePackage{lmodern}
\RequirePackage[sc]{mathpazo}
\linespread{1.05} % adjust line spread for mathpazo font
% microtype for nicer typography
\RequirePackage{microtype}

% semi-bold type (for subsections and paragraphs)
\newcommand*{\sbdefault}{sb}
\DeclareRobustCommand{\sbseries}{%
    \not@math@alphabet\sbseries\relax
    \fontseries\sbdefault\selectfont}

\DeclareTextFontCommand{\textsb}{\sbseries}

\addtokomafont{subsection}{\sbseries}
\addtokomafont{subsubsection}{\sbseries}
\addtokomafont{paragraph}{\sbseries}

%% SPACING

\RedeclareSectionCommands[
    beforeskip= .7em plus .6em minus .3em
]{paragraph}

%% PAGE LAYOUT
\KOMAoptions{
    fontsize=11pt,
    paper=a4, 
    twoside=false,
    titlepage=true,
    headinclude=true,
    footinclude=true,
}

\RequirePackage[left=4.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm]{geometry}

\RequirePackage[onehalfspacing]{setspace}

\clubpenalty=10000 % prevent orphans
\widowpenalty=10000 % prevent widows

%% Styling of titlepage

\RequirePackage{titling}
\setkomafont{title}{\huge\sffamily\bfseries}

%internal commands
\renewcommand{\maketitle}{%
\begin{titlepage}
    \overfullrule=0pt
    \vspace*{1.2\baselineskip}
    {\noindent\huge\sffamily\bfseries\thetitle}
    \\ \vspace{1cm}
    {\noindent\normalfont\large\textit{\@subtitle}}
    
    \vspace{10em}
    \begin{center}		
        {\titlefont\large --- \thekind\ ---\par}
    
    
        {\large \ifcurrentbaselanguage{English}{at the}{an der} \\\ifcurrentbaselanguage{English}{Dortmund University of Applied Sciences and Arts}{Fachhochschule Dortmund} \\ 
        \ifcurrentbaselanguage{English}{at the Department of}{im Fachbereich} \thedepartment\\ \ifcurrentbaselanguage{English}{in the field of }{im Studiengang} \thestudyprogramme\\ \ifcurrentbaselanguage{English}{with the specialization}{mit der Vertiefung} \thespecialization}

        \vspace{1cm}
        {\large \ifcurrentbaselanguage{English}{for the attainment of the academic degree}{zur Erlangung des akademischen Grades} \\ \thedegree}
    \end{center}

    \vfill
    
    \begin{tabular}{l l}
        \ifcurrentbaselanguage{English}{Author}{Eingereicht von}: & \theauthor \\
        \ifcurrentbaselanguage{English}{Student ID}{Matrikelnummer}: & \theidnumber \\
        \ifcurrentbaselanguage{English}{Date of birth}{Geburtstag}: & \thebirthday \\
        \\
        \ifcurrentbaselanguage{English}{Reviewer}{Erstprüfer/-in}: & \thereviewerone \\
        \ifcurrentbaselanguage{English}{Second Reviewer}{Zweitprüfer/-in}: & \thereviewertwo \\
    \end{tabular}
\end{titlepage}
}

%% PACKAGES
\RequirePackage{amssymb}
\RequirePackage{babel}
\RequirePackage{csquotes}

\RequirePackage[shortcuts]{extdash}

% tables
\RequirePackage{booktabs}
\RequirePackage{longtable}
\RequirePackage{array}

\RequirePackage{graphicx}

% appendix
\RequirePackage[toc,title,header]{appendix}
\noappendicestocpagenum

% PDF specific packages
\RequirePackage[hyphens]{url}
\RequirePackage[breaklinks,colorlinks=false,hidelinks]{hyperref}

% Settings for lstlistings
\RequirePackage{xcolor}
\definecolor{FHOrange}{cmyk}{0,70,100,0}
\definecolor{AccentGreen}{cmyk}{6, 0, 100, 32}

\RequirePackage{scrhack} % necessary for listings package
\RequirePackage{listings}

\renewcommand*\lstlistlistingname{\ifcurrentbaselanguage{English}{List of Listings}{Quellcodeverzeichnis}}

\lstset{%
  basicstyle=\ttfamily,
  columns=fullflexible,
  keywordstyle=\bfseries\color{FHOrange},
  stringstyle=\color{AccentGreen}
}